package shopping;

import java.util.ArrayList;
import java.util.Arrays;



class User{
	private String Password ;
	private String Name;
	private double Money;
	ArrayList<Good> ShoppingCart = new ArrayList<Good>();
	
	public User(String name, String password) {
		
		Name = name;
		Password = password;
	}
	
	/*Name Password get&set 可用于后期找回用户名及密码功能*/
	
	public String getPassword() { 
		return Password;
	}

	public void setPassword(String password) { //修改密码
		Password = password;
	}

	public String getName() { //登录与注册时匹配使用
		return Name;
	}

	public void setName(String name) {//修改用户名
		Name = name;
	}

	public double getMoney() {
		return Money;
	}

	public void setMoney(double money) {
		Money = money;
	}

	public String toString() {
		return "Name=" + Name + ", Money=" + Money;
	}

	
	public boolean SCartAdd(Good Goodname){
		ShoppingCart.add(Goodname);
		return true;
	}
	

	
	
}

class Good{
	private String name;
	private double price;
	
	public Good(String name, double price) {
		this.name 

 = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name 

 = name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return "GoodName=" + name + ", price=" + price + "元";
	}
	
}

public class Shop {
	public static void main(String args[]){
		ArrayList<User> UsersList = new ArrayList<User>();
		ArrayList<Good> GoodsList = new ArrayList<Good>();
		User Cheng = new User("Cheng","123456");
		Cheng.setMoney(10000);
		Good apple = new Good("Apple",10);
		Good banana = new Good("Banana",5);
		GoodsList.add(apple);
		Cheng.SCartAdd(apple);
		Cheng.SCartAdd(banana);
		System.out.println(Cheng.toString());
		for(int i=0;i<Cheng.ShoppingCart.size();i++) {
			String alEach=Cheng.ShoppingCart.get(i).toString();
			System.out.println(alEach);
		}
	}

}
