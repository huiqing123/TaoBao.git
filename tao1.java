package taobao;

import java.util.ArrayList;

class User{
	private String Account ; //账号
	private String Password ;//密码
	private String name;
    private int PhoneNumber;
    private String Address;
    ArrayList<String> Users = new ArrayList<String>();//存放用户信息
    
	public User(String account, String password) {
		Account = account;
		Password = password;
	}

	public String getAccount() {
		return Account;
	}
	
	public void setAccount(String account) {
		Account = account;
	}
	
	public String getPassword() {
		return Password;
	}
	
	public void setPassword(String password) {
		Password = password;
	}

	@Override
	public String toString() {
		return "Users [Account=" + Account + ", Password=" + Password + "]";
	}

	public void Modify() {//修改用户个人信息
		
	}
}

class Goods{
	private String name;
	private double price;
	
	public Goods(String name, double price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Goods [name=" + name + ", price=" + price + "]";
	}

	
}

class ShoppingCart{
	
	static double Total;   //总价
	ArrayList<String> b = new ArrayList<String>();
	
	public void AddProduct() {//添加商品
		
	}
	
	public void DeleteProduct() { //删除商品
		
	}
	
	public void ChangeQuantity() {//调整数量
		
	}
	
	public void ClearCart() {//清空购物车 
		
	}
	
	public double CalculateTotal() {//计算总价格
		double sum = 0.0;
		return sum;
	}

	
	
}


public class tao1 {

	public static void main(String[] args) {
		
	}

}
